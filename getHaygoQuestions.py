import traceback
import logging
import pymysql
import json
import uuid
import sys
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(400)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_getHAYGOQuestions(lv_CutitronicsBrandID):

    lv_list = []
    lv_statement = "SELECT QuestionKey, QuestionSet, CutitronicsBrandID, QuestionText FROM ClientSurveyQuestion WHERE EnabledFlag = 'True' AND CutitronicsBrandID = %(CutitronicsBrandID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})

            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500



def fn_getQuestionScore(lv_QuestionKey):

    lv_list = []
    lv_statement = "SELECT QuestionKey, Answer, count(Answer) FROM ClientSurveyFeedback WHERE QuestionKey = %(QuestionKey)s GROUP BY Answer"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'QuestionKey': lv_QuestionKey})
            
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def lambda_handler(event, context):
    """
    getHaygoQuestions

    Gets HAYGO Questions for a Brand
    """
    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Return block

    getHaygoQuestions_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:

        # Variables
        body = event.get('multiValueQueryStringParameters')

        lv_CutitronicsBrandID = body.get('CutitronicsBrandID')[0]

        '''
        1. Does Brand Exist?
        '''

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        if lv_BrandType == 400:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            getHaygoQuestions_out['Status'] = "Error"
            getHaygoQuestions_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getHaygoQuestions_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getHaygoQuestions_out)
            }

        '''
        2. Does brand have HAYGO Questions set up?
        '''

        lv_HAYGO_Qs = fn_getHAYGOQuestions(lv_CutitronicsBrandID)


        if lv_HAYGO_Qs == 400:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not found any questions in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            getHaygoQuestions_out['Status'] = "Error"
            getHaygoQuestions_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getHaygoQuestions_out['ErrorDesc'] = "Supplied brandID does not have any questions in the BO Database"

            # Lambda response
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getHaygoQuestions_out)
            }

        '''
        3. Get haygo question score
        '''
        lv_Questions = []

        for lv_Question in lv_HAYGO_Qs:

            lv_QuestionKey = lv_Question.get("QuestionKey")

            lv_Scores = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 'Total': 0}
            lv_Question_Score = fn_getQuestionScore(lv_QuestionKey)

            if lv_Question_Score != 400:

                for lv_Score in lv_Question_Score:
                    lv_Scores[lv_Score.get('Answer')] = lv_Score.get('count(Answer)')
                    lv_Scores['Total'] = lv_Scores.get('Total') + lv_Score.get('count(Answer)')

                # Build question output
                lv_QuestionReturn = {
                    "QuestionKey": lv_Question.get('QuestionKey'),
                    "QuestionText": lv_Question.get('QuestionText'),
                    "FeedbackScore": lv_Scores
                }

                lv_Questions.append(lv_QuestionReturn)
            else:
                
                logger.warning("")
                logger.warning("Cannot find feedback associated with question '{QuestionKey}' in the back office".format(QuestionKey=lv_QuestionKey))
                logger.warning("")

                lv_Questions.append(lv_Question)

        '''
        4. Build Output
        '''

        getHaygoQuestions_out['ServiceOutput']['CutitronicsBrandID'] = lv_CutitronicsBrandID
        getHaygoQuestions_out['ServiceOutput']['Questions'] = lv_Questions

        Connection.close() 

        logger.info("")
        logger.info("Successful execution, return to caller: '{Return}'".format(Return=json.dumps(getHaygoQuestions_out)))
        logger.info("")

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getHaygoQuestions_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        getHaygoQuestions_out['Status'] = "Error"
        getHaygoQuestions_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        getHaygoQuestions_out['ErrorDesc'] = "CatchALL"
        getHaygoQuestions_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getHaygoQuestions_out)
        }


# class context:
#     def __init__(self):
#         self.function_name = "getHaygoQs"

# context = context()
# event = {'multiValueQueryStringParameters': {"CutitronicsBrandID": ["0001"]}}

# x = lambda_handler(event, context)

# print(x)